# Aidon AMS data parser
NOTE: This is a clone from https://github.com/skagmo/meter_reading.git, credits for the code to Jon Petter Skagmo 

Useful link:

* https://www.hjemmeautomasjon.no/forums/topic/5066-guide-lese-aidon-han-med-raspberry-pi-og-visualisere-i-grafana/

Use with an M-Bus dongle such as [this one](https://www.aliexpress.com/item/USB-to-MBUS-slave-module-MBUS-master-slave-communication-debugging-bus-monitor-TSS721-No-spontaneity-Self/32894249052.html).

### aidon_obis.py
A class for decoding HDLC and extracting OBIS fields. Requires python module crcmod (sudo pip install crcmod).
*NB: This is not a full COSEM parser, as the number of OBIS fields and their sequence is assumed to be as on a Aidon meter.*
https://www.nek.no/wp-content/uploads/2019/02/Aidon-HAN-Interface-Description-v11A-ID-34331.pdf


### aidon_test.py
Test output. <br/>
```
./aidon_test.py <port>
./aidon_test.py /dev/ttyUSB0
```

### aidon_forward.py
Forward to influxdb and Home Assistant.
Forwarding to only one of them is possible. Just omit the influx* or hass* arguments.
<br/>
Will generate sensors in Home assistant named `sensor.aidon_*`. Not done with a component, so these sensors will disappear on Home Assistant restart, and can't be renamed.
<br/>
For InfluxDB, readings are placed in the provided database under `voltage`, `current`, `power` and `energy`, and key `dev` holds device name (which begins with aidon).

```
python aidon_forward.py \
/dev/ttyUSB0 \
--influx_host http://localhost:8086 \
--influx_db metering \
--hass_host https://myhass.com:8123 \
--hass_token abcdefgh0123456789
```

### aidon_read.py
Parses preliminary/old protocol used by Hafslund. <br/>
`./aidon_read.py /dev/ttyUSB0` <br/>


### Install everything

* wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
* echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
* sudo apt-get update
* sudo apt-get install -y grafana
* sudo /bin/systemctl enable grafana-server
* sudo /bin/systemctl start grafana-server

* sudo apt install influxdb
* sudo apt install influxdb-client

* sudo pip install pyserial
* sudo pip install crcmod
* sudo pip install requests==2.7.0

* sudo mkdir /opt/meter_reading
* sudo cp aidon_forward.py aidon_obis.py hass_influx.py /opt/meter_reading/
* sudo cp han.service /lib/systemd/system/
* sudo chmod 644 /lib/systemd/system/han.service
* sudo systemctl daemon-reload
* sudo systemctl enable han.service
* sudo systemctl start han

